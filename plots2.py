import pandas
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import sys


#python plots2.py loc_1.csv loc_2.csv loc_3.csv loc_4.csv loc_5.csv loc_6.csv loc_7.csv loc_8.csv loc_9.csv loc_10.csv loc_11.csv loc_12.csv loc_13.csv loc_14.csv loc_15.csv loc_16.csv loc_17.csv loc_18.csv loc_19.csv loc_20.csv

#df = pandas.read_csv('rewards_loc1.csv')
#print(df['cell_mptcp_tput'].describe())

#x = np.mean(df['cell_mptcp_tput'])
#print(x)
#y = np.mean(df['cell_mptcp_rtt'])

def model_tput(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 'yes']
	rtt = (values['cell_mptcp_rtt'])
	tput = (values['cell_mptcp_tput'])
	return rtt, tput

def default_tput(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 'no']
	rtt = (values['cell_mptcp_rtt'])
	tput = (values['cell_mptcp_tput'])
	return rtt, tput

def CI_model(rtt, tput, confidence = 0.95):
	std_err_x = st.sem(tput)
	n_x = len(tput)
	h_x = std_err_x * st.t.ppf((1 + confidence) / 2, n_x - 1)
	std_err_y = st.sem(rtt)
	n_y = len(rtt)
	h_y = std_err_y * st.t.ppf((1 + confidence) / 2, n_y - 1)
	return h_x, h_y

def model_lossrate(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 'yes']
	lossrate = (values['cell_udp_lossrate'])
	tput = (values['cell_mptcp_tput'])
	return lossrate, tput

def default_lossrate(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 'no']
	lossrate = (values['cell_udp_lossrate'])
	tput = (values['cell_mptcp_tput'])
	return lossrate, tput

def CI_model_lossrate(lossrate, tput, confidence = 0.95):
	std_err_x = st.sem(tput)
	n_x = len(tput)
	h_x = std_err_x * st.t.ppf((1 + confidence) / 2, n_x - 1)
	std_err_y = st.sem(lossrate)
	n_y = len(lossrate)
	h_y = std_err_y * st.t.ppf((1 + confidence) / 2, n_y - 1)
	return h_x, h_y

def main():
	for filename in sys.argv[1:]:
		df = pandas.read_csv(filename)

		rtt_model, tput_model = model_tput(df)
		rtt_default, tput_default = default_tput(df)

		lossrate_model, tput2_model = model_lossrate(df)
		lossrate_default, tput2_default = default_lossrate(df)

		x = np.mean(tput_model)
		y = np.mean(rtt_model)
		h_x, h_y = CI_model(rtt_model, tput_model)
		
		x2 = np.mean(tput_default)
		y2 = np.mean(rtt_default)
		h_x2, h_y2 = CI_model(rtt_default, tput_default)

		x3 = np.mean(tput2_model)
		y3 = np.mean(lossrate_model)
		h_x3, h_y3 = CI_model(lossrate_model, tput2_model)
		
		x4 = np.mean(tput2_default)
		y4 = np.mean(lossrate_default)
		h_x4, h_y4 = CI_model(lossrate_default, tput2_default)
		
		fig, ax = plt.subplots(3)

		#plt.scatter(x, y, marker='X', data=df)
		#fig, ax = plt.subplots() # use to plot separately
		plt.errorbar(y2, x2, xerr=h_y2, yerr=h_x2, fmt='x', label='%s'%(filename))
		plt.errorbar(y, x, xerr=h_y, yerr=h_x, fmt='-o')
		plt.legend()
		plt.xlabel('Latency (MS)')
		plt.ylabel('MPTCP Throughput (Mbps)')
		plt.savefig("rtt_tput_%s.png" %(filename), bbox_inches='tight', dpi = 600)

		#fig2, ax2 = plt.subplots() # use to plot separately
		#plt.errorbar(y3, x3, xerr=h_y3, yerr=h_x3, fmt='-o', label='Contextual-MPTCP-PM')
		#plt.errorbar(y4, x4, xerr=h_y4, yerr=h_x4, fmt='x', label='fullmesh-MPTCP-PM')
		#plt.legend()
		# plt.xlabel('Lossrate')
		# plt.ylabel('Throughput (Mbps)')
		# plt.annotate("Contextual PM", (y3-0.008, x3+0.01))
		# plt.annotate("Fullmesh PM", (y4+0.001, x4+0.01))
		# plt.savefig("RW_lossrate_tput.png", bbox_inches='tight', dpi = 600)
main()