import pandas as pd
import matplotlib.pyplot as plt
data1 = pd.read_csv('loc_1.csv')
data2 = pd.read_csv('loc_2.csv')
data3 = pd.read_csv('loc_3.csv')
data4 = pd.read_csv('loc_4.csv')
data5 = pd.read_csv('loc_5.csv')
data6 = pd.read_csv('loc_6.csv')
data7 = pd.read_csv('loc_7.csv')
data8 = pd.read_csv('loc_8.csv')
data9 = pd.read_csv('loc_9.csv')
data10 = pd.read_csv('loc_10.csv')
data11 = pd.read_csv('loc_11.csv')
data12 = pd.read_csv('loc_12.csv')
data13 = pd.read_csv('loc_13.csv')
data14 = pd.read_csv('loc_14.csv')
data15 = pd.read_csv('loc_15.csv')
data16 = pd.read_csv('loc_16.csv')
data17 = pd.read_csv('loc_17.csv')
data18 = pd.read_csv('loc_18.csv')
data19 = pd.read_csv('loc_19.csv')
data20 = pd.read_csv('loc_20.csv')

data_group1 = [data1.cell_tput, data1.wifi_tput]
data_group2 = [ data2.cell_tput, data2.wifi_tput]
data_group3 = [ data3.cell_tput, data3.wifi_tput]
data_group4 = [ data4.cell_tput, data4.wifi_tput]
data_group5 = [ data5.cell_tput, data5.wifi_tput]
data_group6 = [ data6.cell_tput, data6.wifi_tput]
data_group7 = [ data7.cell_tput, data7.wifi_tput]
data_group8 = [data8.cell_tput, data8.wifi_tput]
data_group9 = [ data9.cell_tput, data9.wifi_tput]
data_group10 = [ data10.cell_tput, data10.wifi_tput]
data_group11 = [data11.cell_tput, data11.wifi_tput]
data_group12 = [ data12.cell_tput, data12.wifi_tput]
data_group13 = [ data13.cell_tput, data13.wifi_tput]
data_group14 = [ data14.cell_tput, data14.wifi_tput]
data_group15 = [data15.cell_tput, data15.wifi_tput]
data_group16 = [data16.cell_tput, data16.wifi_tput]
data_group17 = [data17.cell_tput, data17.wifi_tput]
data_group18 = [data18.cell_tput, data18.wifi_tput]
data_group19 = [data19.cell_tput, data19.wifi_tput]
data_group20 = [data20.cell_tput, data20.wifi_tput]
data2 = [data1.wifi_tput, data2.wifi_tput, data3.wifi_tput]
xlabel = ['Amherst,MA(1)', 'Amherst,MA(2)', 'Amherst,MA(3)', 'Amherst,MA(4)','Amherst,MA(5)', 'Boston,MA(1)', 'Boston,MA(2)', 'Boston,MA(3)', 'Boston,MA(4)', 'Boston,MA(5)', 'Boston,MA(6)', 'Boston,MA(7)', 'Boston,MA(8)', 'Santa Babara,CA(1)', 'Santa Babara,CA(2)', 'Santa Babara,CA(3)', 'Los Angeles,CA', 'Washington,D.C.', 'Princeton,NJ', 'Philadelphia,PA']
#x21abel = ['Wifi-A', 'Wifi-B', 'Wifi-C']

# fig, ax = plt.subplots(2)
# ax[0].set_xticklabels(x1label)
# ax[0].set_ylabel('Throuput (Mbps)')
# ax[0].boxplot(data, notch=True)
# 
# ax[1].set_xticklabels(x21abel)
# ax[1].set_ylabel('Throuput (Mbps)')
# ax[1].boxplot(data2, notch=True)
boxprops = dict(linestyle='--', linewidth=1.5, color='black')

#fig, ax = plt.subplots(figsize=(15,4))

fig, ax = plt.subplots(figsize=(8,2.5))

ax.boxplot(data_group1, positions = [1, 2], showfliers=False, notch=True, widths =  0.8)
ax.boxplot(data_group2, positions = [4, 5],showfliers=False, notch=True, widths =   0.8)
ax.boxplot(data_group3, positions = [7, 8],showfliers=False, notch=True, widths =   0.8)
ax.boxplot(data_group4, positions = [10, 11],showfliers=False, notch=True, widths = 0.8)
#ax.boxplot(data_group5, positions = [13, 14],showfliers=False, notch=True, widths = 0.8)
#ax.boxplot(data_group6, positions = [16, 17],showfliers=False, notch=True, widths = 0.8)
#ax.boxplot(data_group7, positions = [19, 20],showfliers=False, notch=True, widths = 0.8)
ax.boxplot(data_group8, positions = [13, 14],showfliers=False, notch=True, widths = 0.8)
ax.boxplot(data_group9, positions = [16, 17],showfliers=False, notch=True, widths = 0.8)
ax.boxplot(data_group10, positions = [19, 20],showfliers=False, notch=True, widths= 0.8)

#ax.boxplot(data_group11, positions = [31, 32], showfliers=False, notch=True, widths = 0.8)
#ax.boxplot(data_group12, positions = [34, 35],showfliers=False, notch=True, widths =  0.8)
#ax.boxplot(data_group13, positions = [37, 38],showfliers=False, notch=True, widths =  0.8)
ax.boxplot(data_group14, positions = [22, 23],showfliers=False, notch=True, widths =  0.8)
#ax.boxplot(data_group15, positions = [43, 44], showfliers=False, notch=True, widths = 0.8)
ax.boxplot(data_group16, positions = [25, 26],showfliers=False, notch=True, widths =  0.8)
#ax.boxplot(data_group17, positions = [49, 50],showfliers=False, notch=True, widths =  0.8)
ax.boxplot(data_group19, positions = [28, 29],showfliers=False, notch=True, widths =  0.8)
ax.boxplot(data_group20, positions = [31, 32],showfliers=False, notch=True, widths =  0.8)
ax.boxplot(data_group18, positions = [34, 35],showfliers=False, notch=True, widths =  0.8)

ax.set_xticks([1.5, 4.5, 7.5, 10.5, 13.5, 16.5, 19.5,22.5,25.5,28.5,31.5,34.5])
ax.set_xlim(0,36)

#ax.set_xticklabels(['Amherst,MA(1)', 'Amherst,MA(2)', 'Amherst,MA(3)', 'Amherst,MA(4)','Amherst,MA(5)', 'Boston,MA(1)', 'Boston,MA(2)', 'Boston,MA(3)','Boston,MA(4)', 'Boston,MA(5)', 'Boston,MA(6)', 'Boston,MA(7)', 'Boston,MA(8)', 'Santa Babara,CA(1)', 'Santa Babara,CA(2)', 'Santa Babara,CA(3)', 'Los Angeles,CA', 'Washington,D.C.', 'Princeton,NJ', 'Philadelphia,PA'])

ax.set_xticklabels(['Amherst,MA(1)', 'Amherst,MA(2)', 'Amherst,MA(3)', 'Boston,MA(1)', 'Boston,MA(2)', 'Boston,MA(3)', 'Santa Babara,CA(1)', 'Santa Babara,CA(2)', 'Los Angeles,CA', 'Washington,D.C.', 'Princeton,NJ', 'Philadelphia,PA'])
ax.set_ylabel('Throuput (Mbps)',fontsize=7)
# ax.legend([bp1["boxes"][0], bp2["boxes"][0]], ['A', 'B'], loc='upper right')

ax.legend(['LTE', 'Wifi'], loc='upper right')
#ax.boxplot(data2, notch=True)
plt.xticks(rotation=70, fontsize=7)
plt.yticks(fontsize=7)

plt.savefig("boxplot_12.png", bbox_inches='tight', dpi = 600)