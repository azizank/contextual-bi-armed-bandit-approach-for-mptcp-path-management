import pandas
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
from pylab import rcParams
import sys
import csv
from itertools import zip_longest
import matplotlib as mpl
from itertools import cycle
from matplotlib import lines


#python analysis.py loc_1.csv loc_2.csv loc_3.csv loc_4.csv loc_5.csv loc_6.csv loc_7.csv loc_8.csv loc_9.csv loc_10.csv loc_11.csv loc_12.csv loc_13.csv loc_14.csv loc_15.csv loc_16.csv loc_17.csv loc_18.csv loc_19.csv loc_20.csv
def model_tput(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 1]
	rtt = (values['cell_mptcp_rtt'])
	tput = (values['cell_mptcp_tput'])
	return rtt, tput

def default_tput(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 0]
	rtt = (values['cell_mptcp_rtt'])
	tput = (values['cell_mptcp_tput'])
	return rtt, tput

def CI_model(rtt, tput, confidence = 0.95):
	std_err_x = st.sem(tput)
	n_x = len(tput)
	h_x = std_err_x * st.t.ppf((1 + confidence) / 2, n_x - 1)
	std_err_y = st.sem(rtt)
	n_y = len(rtt)
	h_y = std_err_y * st.t.ppf((1 + confidence) / 2, n_y - 1)
	return h_x, h_y

def wifi_tput_percentage(df):
	tput_whole = df['cell_tput'].sum() + df['wifi_tput'].sum()
	return 100 * df['wifi_tput'].sum() / tput_whole

def wifi_rtt_percentage(df):
	rtt_whole = df['cell_rtt'].sum() + df['wifi_rtt'].sum()
	return 100 * df['wifi_rtt'].sum() / rtt_whole

def percentage_wifi_higher_than_lte(df):
	values = df.loc[df['cell_tput_higher_than_wifi'] == 0]
	wifi_higher = len(values)
	percentage = 100 * wifi_higher / len(df['cell_tput_higher_than_wifi'])
	return percentage

def wifi_loss_rate(df):
	loss_whole = (df['wifi_udp_lossrate'].mean()*100)
	return loss_whole

def lte_loss_rate(df):
	loss_whole = (df['cell_udp_lossrate'].mean())
	return loss_whole * 100

def fix_hist_step_vertical_line_at_end(ax):
    '''
    Fix hist step vertical line at end for axes `ax`.
    '''
    axpolygons = [poly for poly in ax.get_children(
    ) if isinstance(poly, mpl.patches.Polygon)]
    for poly in axpolygons:
        poly.set_xy(poly.get_xy()[:-1])

def main():
	script = sys.argv[0]
	wifi_tput = []
	wifi_higher = []
	wifi_lossrate = []
	lte_lossrate = []
	wifi_rtt_per_higher = []
	avg_wifi_rtt = []
	avg_lte_rtt = []
	avg_wifi_tput = []
	avg_lte_tput = []
	avg_cell_cqi = []
	avg_cell_rsrq = []
	mdn_cell_rsrq = []
	count = 0
	linestyle = ['solid', 'dashed', 'dashdot',(0, ()),(0, (1, 10)), (0, (1, 5)), (0, (1, 1)),(0, (5, 10)),(0, (5, 5)),
	(0, (5, 1)), (0, (3, 10, 1, 10)), (0, (3, 5, 1, 5)),
	(0, (3, 1, 1, 1)), 	(0, (3, 10, 1, 10, 1, 10)), (0, (3, 5, 1, 5, 1, 5)),
 	(0, (3, 1, 1, 1, 1, 1)), (0, (3, 2, 1, 2, 1, 2)), (0, (3, 4, 1, 4, 1, 4)), (0, (3, 1, 2, 1, 2, 1)),(0, (3, 5, 1, 5, 2, 1))]
 	#marker = ['.',',','o','v','^','<','>','1','2','3','4','s','p','*','h','H','+','x','D','d','|','_']

	for filename in sys.argv[1:]:
		df = pandas.read_csv(filename)
		df.reset_index(inplace=True)
		#print(df['cell_rsrq'].describe())
		#wifi_lte = df['cell_tput_higher_than_wifi'].values

		df = df.replace(to_replace = 'no', value = 0)
		df = df.replace(to_replace = 'yes', value = 1)

		wifi_lte = df['cell_tput_higher_than_wifi'].values

		#wifi_lte = wifi_lte1['cell_tput_higher_than_wifi'].replace('yes', 1)
		#print(df)
		#print(st.find_repeats(wifi_lte))
		
		rtt_model, tput_model = model_tput(df)
		rtt_default, tput_default = default_tput(df)


		Wifi_tput_percentage = wifi_tput_percentage(df)

		wifi_tput.append(Wifi_tput_percentage)
		

		wifi_higher_p = percentage_wifi_higher_than_lte(df)
		wifi_higher.append(wifi_higher_p)

		wifi_lossrate_m = wifi_loss_rate(df)
		wifi_lossrate.append(wifi_lossrate_m)

		lte_lossrate_m = lte_loss_rate(df)
		lte_lossrate.append(lte_lossrate_m)

		wifi_rtt_higher_than_lte = wifi_rtt_percentage(df)
		wifi_rtt_per_higher.append(wifi_rtt_higher_than_lte)

		average_wifi_rtt = df['wifi_rtt'].mean()
		avg_wifi_rtt.append(average_wifi_rtt)

		average_lte_rtt = df['cell_rtt'].mean()
		avg_lte_rtt.append(average_lte_rtt)

		average_wifi_tput = df['wifi_tput'].mean()
		avg_wifi_tput.append(average_wifi_tput)

		average_lte_tput = df['cell_tput'].mean()
		avg_lte_tput.append(average_lte_tput)

		average_cell_cqi = df['cell_cqi'].mean()
		avg_cell_cqi.append(average_cell_cqi)

		average_cell_rsrq = st.mode(df['cell_rsrq'])
		avg_cell_rsrq.append(average_cell_rsrq)

		median_cell_rsrq = df['cell_rsrq'].median()
		mdn_cell_rsrq.append(median_cell_rsrq)


		marker = ['.',',','o','v','^','<','>','1','2','3','4','s','p','*','h','H','+','x','D','d','|','_']
		label=['Amherst,MA(1)','Amherst,MA(2)','Amherst,MA(3)','Amherst,MA(4)','Amherst,MA(5)','Boston,MA(1)','Boston,MA(2)','Boston,MA(3)','Boston,MA(4)','Boston,MA(5)','Boston,MA(6)','Boston,MA(7)','Boston,MA(8)', 'Santa Babara,CA(1)','Santa Babara,CA(2)','Santa Babara,CA(3)','Los Angeles,CA','Washington,D.C.','Princeton,NJ','Philadelphia,PA']
		X4 = df['cell_tput']
		n4 = np.arange(1,len(X4)+1) / np.float(len(X4))
		Xs4 = np.sort(X4)
		plt.figure(4)
		plt.step(Xs4, n4, label = label[count], marker=marker[count], markevery=500)
		plt.legend(loc="best", fontsize=7.5) 
		plt.xlabel('Link Capacity (Mbps)')
		plt.ylabel('CDF')
		count = count + 1
		plt.savefig("cdf_link_capacity_v2.png", bbox_inches='tight', dpi = 600)


main()