import pandas
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import sys

fig, ax = plt.subplots(3, sharex=True, sharey=True)
df = pandas.read_csv('A.csv')
contextual = df.loc[df['cell_tput_higher_than_wifi'] == 'yes']
fullmesh = df.loc[df['cell_tput_higher_than_wifi'] == 'no']
values1 = contextual['cell_mptcp_tput'].groupby(contextual['cell_mptcp_rtt']).unique().apply(pandas.Series)
values2 = fullmesh['cell_mptcp_tput'].groupby(fullmesh['cell_mptcp_rtt']).unique().apply(pandas.Series)
max_tput_contextual = values1.max(axis=1)
max_tput_fullmesh = values2.max(axis=1)
ax[0].plot(max_tput_contextual, 'ro',label='cont_locA')
ax[0].plot(max_tput_fullmesh, 'kx', label='full_locA')
ax[0].legend()
df = pandas.read_csv('B.csv')
contextual = df.loc[df['cell_tput_higher_than_wifi'] == 'yes']
fullmesh = df.loc[df['cell_tput_higher_than_wifi'] == 'no']
values1 = contextual['cell_mptcp_tput'].groupby(contextual['cell_mptcp_rtt']).unique().apply(pandas.Series)
values2 = fullmesh['cell_mptcp_tput'].groupby(fullmesh['cell_mptcp_rtt']).unique().apply(pandas.Series)
max_tput_contextual = values1.max(axis=1)
max_tput_fullmesh = values2.max(axis=1)
ax[1].plot(max_tput_contextual, 'ro', label='cont_locB')
ax[1].plot(max_tput_fullmesh, 'kx', label='full_locB')
ax[1].legend()
df = pandas.read_csv('C.csv')
contextual = df.loc[df['cell_tput_higher_than_wifi'] == 'yes']
fullmesh = df.loc[df['cell_tput_higher_than_wifi'] == 'no']
values1 = contextual['cell_mptcp_tput'].groupby(contextual['cell_mptcp_rtt']).unique().apply(pandas.Series)
values2 = fullmesh['cell_mptcp_tput'].groupby(fullmesh['cell_mptcp_rtt']).unique().apply(pandas.Series)
max_tput_contextual = values1.max(axis=1)
max_tput_fullmesh = values2.max(axis=1)
ax[2].plot(max_tput_contextual, 'ro', label='cont_locC')
ax[2].plot(max_tput_fullmesh, 'kx', label='full_locC')
ax[2].legend()
plt.xlim(650,0)
fig.savefig("max_tput.png", bbox_inches='tight', dpi = 600)